package com.epam.bubnii.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class ChromeDriverManager extends AbstractDriverManager {

    private static Logger LOGGER = LogManager.getLogger(ChromeDriverManager.class);

    @Override
    protected void createWebDriver() {
        LOGGER.info("Create Chrome web driver");
        webDriver = new ChromeDriver(prepareOptions());
        webDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    private static ChromeOptions prepareOptions(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-infobars");
        return options;
    }
}
