package com.epam.bubnii.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DriverManagerFactory {

    private static Logger LOGGER = LogManager.getLogger(AbstractDriverManager.class);

    public static AbstractDriverManager getDriverManager(DriverType type){
        AbstractDriverManager driverManager;
        switch (type){
            case CHROME:
                LOGGER.info("Create Chrome driver manager");
                driverManager = new ChromeDriverManager();
                break;
            default:
                LOGGER.info("Create default driver manager");
                driverManager = new ChromeDriverManager();
        }
        return driverManager;
    }
}
