package com.epam.bubnii;

import com.epam.bubnii.bo.AuthorizationBo;
import com.epam.bubnii.bo.GmailBo;
import com.epam.bubnii.entity.Email;
import com.epam.bubnii.entity.User;
import com.epam.bubnii.utils.AbstractDriverManager;
import com.epam.bubnii.utils.DriverManagerFactory;
import com.epam.bubnii.utils.DriverType;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.util.Iterator;
import java.util.stream.Stream;

@Listeners({GmailListener.class})
public class AuthorizationPageTest {

    protected static String USER_EMAIL = "testacount968@gmail.com";
    protected static String USER_PASSWORD = "tao1234123";
    protected static String USER_EMAIL_RECEIVER = "bubniy1995@gmail.com";
    protected static String USER_EMAIL_CC = "testacount969@gmail.com";
    protected static String USER_EMAIL_BCC = "saprnulp@gmail.com";
    protected static WebDriver webDriver;
    protected static AbstractDriverManager driverManager;
    private AuthorizationBo authorizationBo;
    private GmailBo gmailBo;

    @BeforeClass(alwaysRun = true)
    public static void setUp() {
        driverManager = DriverManagerFactory.getDriverManager(DriverType.CHROME);
        webDriver = driverManager.getWebDriver();
    }

    @DataProvider(parallel = true)
    private Iterator<Object[]> users() {
        return Stream.of(
                new Object[]{"testacount968@gmail.com", "tao1234123"},
                new Object[]{"testacount968@gmail.com", "tao1234123"},
                new Object[]{"testacount968@gmail.com", "tao1234123"},
                new Object[]{"testacount968@gmail.com", "tao1234123"}
        ).iterator();
    }

    @Test(dataProvider = "users")
    public void testSendEmail(String userEmail, String userPassword) {
        webDriver.get("https://mail.google.com/mail");
        authorizationBo = new AuthorizationBo(webDriver);
        gmailBo = new GmailBo(webDriver);
        authorizationBo.login(new User()
                .setEmail(userEmail)
                .setPassword(userPassword));
        gmailBo.sendMessage(new Email()
                .setReceiver(USER_EMAIL_RECEIVER)
                .setReceiverCopy(USER_EMAIL_CC)
                .setReceiverHiddenCopy(USER_EMAIL_BCC)
                .setSubject("EPAM")
                .setMessage("ky-ky"));
    }

    @AfterClass
    public static void tearDown() {
        webDriver.close();
    }

}
