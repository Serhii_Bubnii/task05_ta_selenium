package com.epam.bubnii;

import com.epam.bubnii.utils.AbstractDriverManager;
import com.epam.bubnii.utils.DriverManagerFactory;
import com.epam.bubnii.utils.DriverType;
import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class GmailListener implements ITestListener {
    private static final Logger LOGGER = LogManager.getLogger(GmailListener.class);

    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenShotsPNG(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    @Attachment(value = "{0}", type = "text/plain")
    public static String saveTextLog(String message) {
        return message;
    }

    private static String getTestMethodName(ITestResult result) {
        return result.getMethod().getConstructorOrMethod().getName();
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        LOGGER.info("The name of the testcase Skipped is :" + getTestMethodName(result));
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("The name of the testcase passed is :" + getTestMethodName(result));
    }

    @Override
    public void onTestFailure(ITestResult result) {
        LOGGER.info("Test Failure!!! Screenshot captured for test case: " + getTestMethodName(result));
        saveScreenShotsPNG(DriverManagerFactory.getDriverManager(DriverType.CHROME).getWebDriver());
        saveTextLog(getTestMethodName(result) + " and screenshot taken!");
    }

}
